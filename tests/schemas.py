# test schemas

_TYPE_INT = {"type": "integer"}
_TYPE_INT_POSITIVE = {"type": "integer", "minimum": 1}
_TYPE_STRING = {"type": "string"}
_TYPE_DATE = {"type": "string", "format": "date"}
_TYPE_NULL = {"type": "null"}

POST_RECORD_SCHEMA = {
    "type": "object",
    "properties": {
        "record_id": _TYPE_INT_POSITIVE
    },
    "required": ["record_id"],
    "additionalProperties": False,
}

GET_RECORDS_SCHEMA = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": _TYPE_INT_POSITIVE,
            "value": _TYPE_INT,
            "create_date": _TYPE_DATE,
            "apply_date": _TYPE_DATE,
            "comment": _TYPE_STRING
        },
        "required": ["id", "value", "create_date", "apply_date", "comment"],
        "additionalProperties": False,
    }
}

POST_CONST_SCHEMA = {
    "type": "object",
    "properties": {
        "const_name": _TYPE_STRING
    },
    "required": ["const_name"],
    "additionalProperties": False,
}

GET_CONSTS_SCHEMA = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "name": _TYPE_STRING,
            "value": _TYPE_INT,
            "frequency": _TYPE_STRING,
            "comment": _TYPE_STRING,
            "freq_value": {"oneOf": [_TYPE_INT, _TYPE_NULL]}
        },
        "required": ["name", "value", "frequency", "comment"],
        "additionalProperties": False,
    }
}

POST_APPLICABILITY_SCHEMA = {
    "type": "object",
    "properties": {
        "applicability_id": _TYPE_INT_POSITIVE
    },
    "required": ["applicability_id"],
    "additionalProperties": False,
}

GET_APPLICABILITY_SCHEMA = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": _TYPE_INT_POSITIVE,
            "name": _TYPE_STRING,
            "apply_date": _TYPE_DATE,
            "start_date": _TYPE_DATE,
            "end_date": _TYPE_DATE,
        },
        "required": ["id", "name", "apply_date", "start_date", "end_date"],
        "additionalProperties": False,
    }
}
