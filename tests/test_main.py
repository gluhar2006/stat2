import unittest
import requests
from jsonschema import validate
from tests import schemas
from datetime import date

url = 'http://127.0.0.1:5000/'


# warning: clear all db


class ApiTest(unittest.TestCase):
    # default jsons
    recordJson = {
        'value': 100,
        'date': '2001-01-11',
        'comment': 'tests record'
    }

    constJson = {
        'name': 'test const',
        'value': 101,
        'frequency': 'day',
        'comment': 'test const'
    }

    @staticmethod
    def removeApplicabilities():
        appIds = [app['id'] for app in requests.get(url + 'applicability').json()]
        for appId in appIds:
            requests.delete(url + 'applicability/' + str(appId))

    @staticmethod
    def removeRecords():
        recordIds = [r['id'] for r in requests.get(url + 'record').json()]
        for recordId in recordIds:
            requests.delete(url + 'record/' + str(recordId))

    @staticmethod
    def removeConsts():
        constNames = [c['name'] for c in requests.get(url + 'const').json()]
        for constName in constNames:
            requests.delete(url + 'const/' + constName)

    def setUp(self) -> None:
        super().setUp()
        ApiTest.removeRecords()
        ApiTest.removeConsts()
        ApiTest.removeApplicabilities()

    def tearDown(self) -> None:
        super().tearDown()
        ApiTest.removeRecords()
        ApiTest.removeConsts()
        ApiTest.removeApplicabilities()

    def validateAndPrint(self, reply, schema):
        self.assertTrue(1, 1)
        print(reply)
        validate(reply, schema)

    def test_post_record(self):
        r = requests.post(url + 'record', json=self.recordJson)
        self.validateAndPrint(r.json(), schemas.POST_RECORD_SCHEMA)

    def test_get_records(self):
        r = requests.get(url + 'record')
        self.validateAndPrint(r.json(), schemas.GET_RECORDS_SCHEMA)

    def test_patch_record(self):
        rId = requests.post(url + 'record', json=self.recordJson).json()['record_id']
        newData = {
            'value': 150,
            'date': '2003-01-11',
            'comment': 'test record'
        }
        requests.patch(url + 'record/' + str(rId), json=newData)
        applyDateGte = '2002-01-11'
        createDateLte = str(date.today())
        r = requests.get(url + 'record?' + 'create_date_lte=' + createDateLte + '&apply_date_gte=' + applyDateGte)
        # ... fake test, I know
        for key, value in newData.items():
            if key != 'date':
                self.assertEqual(r.json()[-1][key], value)

    def test_delete_record(self):
        rId = requests.post(url + 'record', json=self.recordJson).json()['record_id']
        requests.delete(url + 'record/' + str(rId))
        recordIds = [r['id'] for r in requests.get(url + 'record').json()]
        self.assertNotIn(rId, recordIds)

    def test_post_const(self):
        r = requests.post(url + 'const', json=self.constJson)
        self.validateAndPrint(r.json(), schemas.POST_CONST_SCHEMA)

    def test_get_const(self):
        requests.post(url + 'const', json=self.constJson)
        r = requests.get(url + 'const')
        self.validateAndPrint(r.json(), schemas.GET_CONSTS_SCHEMA)
        for key, value in self.constJson.items():
            self.assertEqual(r.json()[0][key], value)

    def test_patch_const(self):
        constName = requests.post(url + 'const', json=self.constJson).json()['const_name']
        requests.patch(url + 'const/' + constName, json={
            'name': 'test const',
            'value': 102,
            'frequency': 'day',
            'comment': 'test const'
        })
        r = requests.get(url + 'const')
        for key, value in self.constJson.items():
            if key != 'value':
                self.assertEqual(r.json()[0][key], value)
            else:
                self.assertEqual(r.json()[0][key], 102)

    def test_delete_const(self):
        cName = requests.post(url + 'const', json=self.constJson).json()['const_name']
        requests.delete(url + 'const/' + cName)
        constNames = [c['const_name'] for c in requests.get(url + 'const').json()]
        self.assertNotIn(cName, constNames)

    def test_post_applicability(self):
        requests.post(url + 'const', json=self.constJson)
        r = requests.post(url + 'applicability', json={
            'name': self.constJson['name'],
            'start_date': '2019-01-10',
            'end_date': '2019-03-05'
        })
        self.validateAndPrint(r.json(), schemas.POST_APPLICABILITY_SCHEMA)
        r = requests.get(url + 'record')
        recordList = r.json()
        self.assertEqual(len(recordList), 54)
        for record in recordList:
            self.assertEqual(record['value'], self.constJson['value'])

    def test_get_applicability(self):
        requests.post(url + 'const', json=self.constJson)
        requests.post(url + 'applicability', json={
            'name': self.constJson['name'],
            'start_date': '2019-01-10',
            'end_date': '2019-03-05'
        })
        r = requests.get(url + 'applicability')
        self.validateAndPrint(r.json(), schemas.GET_APPLICABILITY_SCHEMA)

    def test_delete_applicability(self):
        requests.post(url + 'const', json=self.constJson)
        appId = requests.post(url + 'applicability', json={
            'name': self.constJson['name'],
            'start_date': '2019-01-10',
            'end_date': '2019-03-05'
        }).json()['applicability_id']
        requests.delete(url + 'applicability/' + str(appId))
        appIds = [app['id'] for app in requests.get(url + 'applicability').json()]
        self.assertEqual(appIds, [])



if __name__ == '__main__':
    unittest.main()
