# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name="stat 2",
    description="Scrum cards service",
    packages=find_packages(exclude=["docs"]),
    zip_safe=False,
    install_requires=[

    ],
)