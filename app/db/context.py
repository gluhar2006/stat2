from typing import Optional, Union

from db import models, dbEngine
from sqlalchemy import insert, update, delete, select, and_, or_, extract
from datetime import date, timedelta
from logging import Logger
from enums import Freq
from calendar import nextmonth, monthrange

logger = Logger('db context')


def getDates(startDate: date, endDate: date, freq: str, freqVal: int) -> list:
    if freq == Freq.day.name:
        return [startDate + timedelta(days=cnt) for cnt in range((endDate - startDate).days + 1)]
    elif freq == Freq.dayOfWeek.name:
        while startDate.weekday() + 1 != freqVal:
            startDate = startDate + timedelta(days=1)
        return [startDate + timedelta(days=cnt) for cnt in range(0, (endDate - startDate).days, 7)]
    elif freq == Freq.week.name:
        return [startDate + timedelta(days=cnt) for cnt in range(0, (endDate - startDate).days, 7)]
    elif freq == Freq.week2.name:
        return [startDate + timedelta(days=cnt) for cnt in range(0, (endDate - startDate).days, 14)]
    elif freq == Freq.weekday.name:
        return [startDate + timedelta(days=cnt) for cnt in range((endDate - startDate).days + 1) if
                (startDate + timedelta(days=cnt)).weekday() < 5]
    elif freq == Freq.weekend.name:
        return [startDate + timedelta(days=cnt) for cnt in range((endDate - startDate).days + 1) if
                (startDate + timedelta(days=cnt)).weekday() > 4]
    elif freq == Freq.month.name or freq == Freq.monthWeekDay.name:
        res = []
        _year, _month = startDate.year, startDate.month

        def getPreparedVal(vYear, vMonth):
            preparedVal = min(monthrange(vYear, vMonth)[1], freqVal)
            if freq == Freq.monthWeekDay.name:
                while date(vYear, vMonth, preparedVal).weekday() > 4:
                    preparedVal -= 1
            return preparedVal

        while date(_year, _month, getPreparedVal(_year, _month)) <= endDate:
            if _year == startDate.year and _month == startDate.month and freqVal < startDate.day:
                _year, _month = nextmonth(_year, _month)
                continue
            res.append(date(_year, _month, getPreparedVal(_year, _month)))
            _year, _month = nextmonth(_year, _month)
        return res


def prepareResp(columnList, valueList):
    res = []
    for row in valueList:
        resRow = []
        for value in row:
            if isinstance(value, date):
                value = str(value)
            resRow.append(value)
        res.append(resRow)
    return [dict(zip(columnList, row)) for row in res]


class DBContext:

    def __init__(self):
        self.engine = dbEngine

        def createMonth(year: int, month: int):
            with self.engine.begin() as con:
                insertSt = insert(models.Month).values(year=year, month=month, end_value=0)
                con.execute(insertSt)

        with self.engine.begin() as con:
            curYear = date.today().year
            selectSt = select([models.Month.year, models.Month.month])
            dbRes = con.execute(selectSt).cursor.fetchall()
            existingPairs = [{'year': row[0], 'month': row[1]} for row in dbRes]
            for _y in range(1, 6):
                for _year in (curYear, curYear + _y):
                    for _month in range(1, 12 + 1):
                        if {'year': _year, 'month': _month} not in existingPairs:
                            createMonth(_year, _month)

    # @dbWrap
    def putRecord(self, value: int, applyDate: date, comment: str):
        with self.engine.begin() as con:
            curDate = date.today()
            insertSt = insert(models.Record).values(value=value, create_date=curDate, apply_date=applyDate,
                                                    comment=comment)
            recordId = con.execute(insertSt).inserted_primary_key[0]
            return recordId

    # @dbWrap
    def updateRecord(self, recordId: int, value: int, applyDate: date, comment: str):
        with self.engine.begin() as con:
            curDate = date.today()
            updateSt = update(models.Record).where(models.Record.id == recordId).values(
                value=value, create_date=curDate, apply_date=applyDate, comment=comment)
            con.execute(updateSt)

    # @dbWrap
    def deleteRecord(self, recordId: int):
        with self.engine.begin() as con:
            deleteSt = delete(models.Record).where(and_(models.Record.id == recordId))
            dbReply = con.execute(deleteSt)
            if dbReply.rowcount != 1:
                raise Exception('delete record problem')

    # @dbWrap
    def getRecords(self, createDateGte: Optional[Union[str, date]] = None,
                   createDateLt: Optional[Union[str, date]] = None, applyDateGte: Optional[Union[str, date]] = None,
                   applyDateLt: Optional[Union[str, date]] = None, oneDayResult: Optional[bool] = None) -> list:
        if oneDayResult:
            applyDateLt = date.fromisoformat(applyDateGte) + timedelta(days=1)
        filters = and_(
            models.Record.create_date >= createDateGte if createDateGte else True,
            models.Record.create_date < createDateLt if createDateLt else True,
            models.Record.apply_date >= applyDateGte if applyDateGte else True,
            models.Record.apply_date < applyDateLt if applyDateLt else True,
        )

        with self.engine.begin() as con:
            selectSt = select([models.Record.id, models.Record.value, models.Record.create_date,
                               models.Record.apply_date, models.Record.comment]).where(filters).order_by(
                models.Record.apply_date.asc()
            )
            dbRes = con.execute(selectSt).cursor.fetchall()

            return prepareResp(selectSt.columns.keys(), dbRes)

    # @dbWrap
    def putConst(self, name: str, value: int, frequency: str, comment: str, freqVal: Optional[int]):
        with self.engine.begin() as con:
            curDate = date.today()
            data = {'name': name, 'create_date': curDate, 'value': value, 'frequency': frequency, 'comment': comment}
            if freqVal is not None:
                data['freq_value'] = freqVal
            insertSt = insert(models.Const).values(**data)
            constId = con.execute(insertSt).inserted_primary_key[0]
            return constId

    # @dbWrap
    def updateConst(self, name: str, value: int, frequency: str, comment: str, freqVal: Optional[int]):
        with self.engine.begin() as con:
            curDate = date.today()
            data = {'create_date': curDate, 'value': value, 'frequency': frequency, 'comment': comment}
            if freqVal is not None:
                data['freq_value'] = freqVal
            updateSt = update(models.Const).values(**data).where(models.Const.name == name)
            con.execute(updateSt)

    # @dbWrap
    def deleteConst(self, name: str):
        with self.engine.begin() as con:
            deleteSt = delete(models.Const).where(models.Const.name == name)
            con.execute(deleteSt)

    # @dbWrap
    def getConsts(self, valueLte: Optional[int] = None, valueGte: Optional[int] = None):
        filters = and_(
            models.Const.value >= valueGte if valueGte else True,
            models.Const.value <= valueLte if valueLte else True,
        )

        with self.engine.begin() as con:
            selectSt = select([models.Const.name, models.Const.value, models.Const.frequency,
                               models.Const.comment, models.Const.freq_value]).where(filters)
            dbRes = con.execute(selectSt).cursor.fetchall()

            return prepareResp(selectSt.columns.keys(), dbRes)

    # @dbWrap
    def putApplicability(self, name: str, startDate: str, endDate: str):
        curDate = date.today()
        with self.engine.begin() as con:
            selectConstSt = select([models.Const.value, models.Const.frequency, models.Const.freq_value]).where(
                models.Const.name == name
            )
            constRes = con.execute(selectConstSt).cursor.fetchone()

            value, freq, freqVal = constRes
            freqVal = freqVal if freqVal is not None else 1

            insertSt = insert(models.Applicability).values(name=name, apply_date=curDate, start_date=startDate,
                                                           end_date=endDate)
            appId = con.execute(insertSt).inserted_primary_key[0]

            datesToApply = getDates(date.fromisoformat(startDate), date.fromisoformat(endDate), freq, freqVal)
            for dateToApply in datesToApply:
                recordInsertSt = insert(models.Record).values(
                    value=value, create_date=curDate, apply_date=dateToApply,
                    comment=f'{freq} for {name} from {str(startDate)} to {str(endDate)}',
                    applicability_id=appId
                )
                con.execute(recordInsertSt)

            return appId

    # @dbWrap
    def getApplicabilityList(self):
        with self.engine.begin() as con:
            selectSt = select([models.Applicability.id, models.Applicability.name, models.Applicability.apply_date,
                               models.Applicability.start_date, models.Applicability.end_date])
            dbRes = con.execute(selectSt).cursor.fetchall()

            return prepareResp(selectSt.columns.keys(), dbRes)

    # @dbWrap
    def deleteApplicability(self, aId: int):
        with self.engine.begin() as con:
            deleteSt = delete(models.Applicability).where(models.Applicability.id == aId)
            con.execute(deleteSt)

    # @dbWrap
    def updateValues(self):
        with self.engine.begin() as con:
            selectMonthSt = select([models.Month.year, models.Month.month]).order_by(
                models.Month.year.asc(), models.Month.month.asc())

            dbReply = con.execute(selectMonthSt).cursor.fetchall()
            yearMonthStartValueList = [{'year': row[0], 'month': row[1]} for row in dbReply]

            value = 0
            for row in yearMonthStartValueList:
                year, month = row.values()

                selectAddValue = select([models.Record.value]).where(and_(
                    extract('year', models.Record.apply_date) == year,
                    extract('month', models.Record.apply_date) == month
                ))
                dbReply = con.execute(selectAddValue).cursor.fetchall()

                if dbReply:
                    for valList in dbReply:
                        for val in valList:
                            value += val

                updateSt = update(models.Month).where(and_(
                    models.Month.year == year,
                    models.Month.month == month
                )).values(end_value=value)
                con.execute(updateSt)

    def getDates(self, values: Optional[str]):
        with self.engine.begin() as con:
            if values == 'years':
                selectSt = select([models.Month.year], distinct=True).order_by(models.Month.year.asc())
            elif values == 'months':
                selectSt = select([models.Month.month], distinct=True).order_by(models.Month.month.asc())
            else:
                selectSt = select([models.Month.year, models.Month.month]).order_by(models.Month.year.asc(),
                                                                                    models.Month.month.asc())
            dbReply = con.execute(selectSt).cursor.fetchall()
            return prepareResp(selectSt.columns.keys(), dbReply)

    def getMonthValues(self, year: int, month: int):
        with self.engine.begin() as con:
            yearPrev = year if month > 1 else year - 1
            monthPrev = month - 1 if month > 1 else 12
            selectSt = select([models.Month.end_value]).where(and_(models.Month.year == yearPrev,
                                                                   models.Month.month == monthPrev))
            try:
                startValue = con.execute(selectSt).cursor.fetchone()[0]
            except Exception:
                startValue = 0
            dateGte = date(year=year, month=month, day=1)
            _nextYear, _nextMonth = nextmonth(year, month)
            dateLt = date(_nextYear, _nextMonth, 1)

            records = self.getRecords(applyDateGte=dateGte, applyDateLt=dateLt)

            res = {}
            for day in range(1, monthrange(year, month)[1] + 1):
                res[day] = {'day_index': day}
                if day == 1:
                    res[day]['day_start'] = startValue
                else:
                    res[day]['day_start'] = res[day - 1]['day_end']
                res[day]['incomes'] = 0
                res[day]['expenses'] = 0
                dayValues = [rec['value'] for rec in records if rec['apply_date'] == str(date(year, month, day))]
                for value in dayValues:
                    if value > 0:
                        res[day]['incomes'] += value
                    else:
                        res[day]['expenses'] += value
                res[day]['day_end'] = res[day]['day_start'] + sum(dayValues)

            return res

    def getStat(self, description: str, applyDateGte: Optional[Union[str, date]] = None,
                applyDateLt: Optional[Union[str, date]] = None):
        with self.engine.begin() as con:
            selectSt = select([models.Record.name, models.Record.value, models.Record.comment, models.Record.apply_date,
                               models.Record.create_date]).where(and_(
                models.Record.comment.ilike(f'%{description}%')),
                models.Record.apply_date >= applyDateGte if applyDateGte else True,
                models.Record.apply_date < applyDateLt if applyDateLt else True,
            ).order_by(models.Record.apply_date.asc())
            dbReply = con.execute(selectSt).cursor.fetchall()
            return prepareResp(selectSt.columns.keys(), dbReply)
