from sqlalchemy import Column, Integer, String, DATE, ForeignKey
from . import dbMeta
from sqlalchemy.ext.declarative import declarative_base

_Base = declarative_base()


class Record(_Base):

    __tablename__ = 'record'
    _Base.metadata = dbMeta

    id = Column(Integer, primary_key=True, autoincrement=True)

    value = Column(Integer, index=True, unique=False, comment='money value')

    create_date = Column(DATE, comment='record create time')

    apply_date = Column(DATE, comment='record apply time')

    comment = Column(String(256))

    name = Column(String(16), ForeignKey('const.name', ondelete='CASCADE'), comment='unique name')

    applicability_id = Column(Integer, ForeignKey('applicability.id', ondelete='CASCADE'))


class Const(_Base):

    __tablename__ = 'const'
    _Base.metadata = dbMeta

    name = Column(String(16), primary_key=True, comment='unique name')

    value = Column(Integer, index=True, unique=False, comment='const value')

    frequency = Column(String(16), comment='value frequency')

    freq_value = Column(Integer, comment='optional value for frequency for month')

    create_date = Column(DATE, comment='const create time')

    comment = Column(String(256))


class Applicability(_Base):

    __tablename__ = 'applicability'
    _Base.metadata = dbMeta

    id = Column(Integer, primary_key=True)

    name = Column(String(16), ForeignKey('const.name', ondelete='CASCADE'))

    apply_date = Column(DATE, comment='apply date')

    start_date = Column(DATE, comment='start date')

    end_date = Column(DATE, comment='end date')


class Month(_Base):

    __tablename__ = 'month'
    _Base.metadata = dbMeta

    year = Column(Integer, primary_key=True)

    month = Column(Integer, primary_key=True)

    start_value = Column(Integer)

    end_value = Column(Integer)
