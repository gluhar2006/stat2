from sqlalchemy import create_engine, MetaData
from config import DB_URL as _dbUrl

dbEngine = create_engine(_dbUrl)
dbMeta = MetaData(bind=dbEngine)
