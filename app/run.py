import os
import sys

from flask import Flask, request, Response

from functions import excWrap as funcWrap
from handlers import record, const, applicability, month, dates, monthValues, stat

app = Flask('stat2')
app.config['SECRET_KEY'] = 'secret!'  # dn wtf is it
# cors = CORS(app, resources=r"/*")
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0  # try to disable cache in browser


# const and app. POST - not restFULL...

@funcWrap
@app.route('/record/<_id>', methods=['PATCH', 'DELETE', 'OPTIONS'])
def recordHandlerFunc(_id):
    recordId = int(_id)
    if request.method == 'PATCH':
        return record.patch(recordId, request)
    elif request.method == 'DELETE':
        return record.delete(recordId)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'PATCH, DELETE, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*',
                                 'Access-Control-Allow-Methods': 'PATCH, DELETE, OPTIONS'})


@funcWrap
@app.route('/record', methods=['GET', 'POST', 'OPTIONS'])
def recordsHandlerFunc():
    if request.method == 'POST':
        return record.post(request)
    elif request.method == 'GET':
        return record.get(request)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'GET, POST, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*', })


def dir_last_updated(folder):
    return str(max(os.path.getmtime(os.path.join(root_path, f))
                   for root_path, dirs, files in os.walk(folder)
                   for f in files))


@funcWrap
@app.route('/const/<name>', methods=['PATCH', 'DELETE', 'OPTIONS'])
def constHandlerFunc(name):
    constName = name
    if request.method == 'PATCH':
        return const.patch(constName, request)
    elif request.method == 'DELETE':
        return const.delete(constName)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'PATCH, DELETE, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*',
                                 'Access-Control-Allow-Methods': 'PATCH, DELETE, OPTIONS'})


@funcWrap
@app.route('/const', methods=['GET', 'POST', 'OPTIONS'])
def constsHandlerFunc():
    if request.method == 'POST':
        return const.post(request)
    elif request.method == 'GET':
        return const.get(request)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'GET, POST, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*', })


@funcWrap
@app.route('/applicability/<_id>', methods=['DELETE', 'OPTIONS'])
def applicabilityHandlerFunc(_id):
    appId = int(_id)
    if request.method == 'DELETE':
        return applicability.delete(appId)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'DELETE, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*',
                                 'Access-Control-Allow-Methods': 'DELETE, OPTIONS'})


@funcWrap
@app.route('/applicability', methods=['GET', 'POST', 'OPTIONS'])
def applicabilitiesHandlerFunc():
    if request.method == 'POST':
        return applicability.post(request)
    elif request.method == 'GET':
        return applicability.get(request)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'GET, POST, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*', })


@funcWrap
@app.route('/month', methods=['POST', 'GET'])
def updateHandlerFunc():
    if request.method == 'POST':
        return month.post(request)
    elif request.method == 'GET':
        return month.get(request)


@funcWrap
@app.route('/month_values', methods=['GET'])
def monthStartHandlerFunc():
    if request.method == 'GET':
        return monthValues.get(request)


@funcWrap
@app.route('/dates', methods=['GET'])
def datesHandlerFunc():
    if request.method == 'GET':
        return dates.get(request)


@funcWrap
@app.route('/statistic', methods=['GET', 'OPTIONS'])
def statHandlerFunc():
    if request.method == 'GET':
        return stat.get(request)
    elif request.method == 'OPTIONS':
        return Response(headers={'Allow': 'GET, OPTIONS', 'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*', })


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    r.headers['Access-Control-Allow-Origin'] = '*'
    return r

if __name__ == '__main__':
    if len(sys.argv) > 1:
        port = sys.argv[1]
    else:
        port = 5000
    app.run(host='0.0.0.0', port=port)
