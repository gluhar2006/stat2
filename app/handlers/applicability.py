import json
from datetime import date

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def delete(appId: int):
    dbContext.deleteApplicability(aId=appId)
    dbContext.updateValues()
    resp = Response(status=204)
    return resp


def post(request):
    data = request.json
    appId = dbContext.putApplicability(name=data['name'],
                                       startDate=data['start_date'],
                                       endDate=data['end_date'])
    dbContext.updateValues()
    resp = Response(response=json.dumps({'applicability_id': appId}), status=201)
    return resp


def get(request):
    res = dbContext.getApplicabilityList()
    resp = Response(response=json.dumps(res), status=200)
    return resp
