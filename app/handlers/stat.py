import json

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def get(request):
    description = request.values.get('description', None)
    applyDateGte = request.values.get('apply_date_gte', None)
    applyDateLt = request.values.get('apply_date_lt', None)
    res = dbContext.getStat(description=description, applyDateLt=applyDateLt, applyDateGte=applyDateGte)
    resp = Response(response=json.dumps(res), status=200)
    return resp
