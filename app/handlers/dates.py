import json

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def get(request):
    res = dbContext.getDates(request.values.get('values'))
    resp = Response(response=json.dumps(res), status=200)
    return resp
