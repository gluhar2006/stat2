import json
from datetime import date

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def get(request):
    today = date.today()
    _year = int(request.values.get('year', today.year))
    _month = int(request.values.get('month', today.month))
    res = dbContext.getMonthValues(year=_year, month=_month)
    resp = Response(response=json.dumps(res), status=200)
    return resp
