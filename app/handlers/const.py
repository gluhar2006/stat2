import json

from flask import Response

from db.context import DBContext
from enums import Freq

dbContext = DBContext()


def patch(constName, request):
    data = request.json
    if 'freq_value' in data and data['freq_value'] is not None:
        data['freqVal'] = data.pop('freq_value')
    else:
        if 'freq_value' in data:
            del data['freq_value']
        data['freqVal'] = None
    data['name'] = constName
    dbContext.updateConst(**data)
    resp = Response(status=204)
    return resp


def delete(constName):
    dbContext.deleteConst(constName)
    dbContext.updateValues()
    resp = Response(status=204)
    return resp


def post(request):
    data = request.json
    name = data['name']
    value = data['value']
    frequency = data['frequency']
    comment = data['comment']
    if 'freq_value' in data and data['freq_value'] is not None:
        data['freqVal'] = data.pop('freq_value')
    else:
        if 'freq_value' in data:
            del data['freq_value']
        data['freqVal'] = None
    freqVal = data['freqVal']
    constId = dbContext.putConst(name=name, value=value, frequency=frequency,
                                 comment=comment, freqVal=freqVal)
    resp = Response(response=json.dumps({'const_name': constId}), status=201)
    return resp


def get(request):
    valueLte = request.values.get('value_lte', None)
    valueGte = request.values.get('value_gte', None)
    res = dbContext.getConsts(valueLte=valueLte, valueGte=valueGte)
    resp = Response(response=json.dumps(res), status=200)
    return resp
