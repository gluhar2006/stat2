import json
from datetime import date

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def patch(recordId: int, request):
    data = request.json
    dbContext.updateRecord(recordId=recordId, value=int(data['value']), applyDate=date.fromisoformat(data['date']),
                           comment=data['comment'])
    resp = Response(status=204)
    return resp


def delete(recordId: int):
    dbContext.deleteRecord(recordId=recordId)
    dbContext.updateValues()
    resp = Response(status=204)
    return resp


def post(request):
    data = request.json
    recordId = dbContext.putRecord(value=int(data['value']), applyDate=date.fromisoformat(data['date']),
                                   comment=data['comment'])
    resp = Response(response=json.dumps({'record_id': recordId}), status=201,
                    headers={'Content-Type': 'application/json',
                             'Access-Control-Allow-Origin': '*',
                             'Access-Control-Allow-Headers': 'application/json'}
                    )
    return resp


def get(request):
    createDateGte = request.values.get('create_date_gte', None)
    createDateLt = request.values.get('create_date_lt', None)
    applyDateGte = request.values.get('apply_date_gte', None)
    applyDateLt = request.values.get('apply_date_lt', None)
    oneDayResult = bool(request.values.get('day', None))
    res = dbContext.getRecords(createDateGte=createDateGte, createDateLt=createDateLt,
                               applyDateGte=applyDateGte, applyDateLt=applyDateLt, oneDayResult=oneDayResult)
    resp = Response(response=json.dumps(res), status=200)
    return resp
