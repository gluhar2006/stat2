import json
from datetime import date
from dateutil.relativedelta import relativedelta

from flask import Response

from db.context import DBContext

dbContext = DBContext()


def post(request):
    dbContext.updateValues()
    return Response(status=202)


def get(request):
    today = date.today()
    _year = int(request.values.get('year', today.year))
    _month = int(request.values.get('month', today.month))
    applyDateGte = date(_year, _month, 1)
    applyDateLt = applyDateGte + relativedelta(months=1)
    res = dbContext.getRecords(applyDateLt=applyDateLt, applyDateGte=applyDateGte)
    resp = Response(response=json.dumps(res), status=200)
    return resp
