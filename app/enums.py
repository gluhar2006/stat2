from enum import Enum

class Freq(Enum):

    day = 1
    dayOfWeek = 2
    week = 3
    week2 = 4
    weekday = 5
    weekend = 6
    month = 7
    monthWeekDay = 8
