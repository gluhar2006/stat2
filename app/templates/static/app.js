var app = angular.module('glApp', []);
app.controller('appCtrl', function($scope, $http) {

  const baseUrl = 'http://127.0.0.1:5010/'

  $scope.freqEnum = ['day', 'dayOfWeek', 'week', 'week2', 'weekday', 'weekend', 'month', 'monthWeekDay'];

  $scope.openNav = function() {
    document.getElementById("sideNav").style.width = "100%";
    document.getElementById('sideNav').style.backgroundColor = '#E8F6FF';
    $scope.editConstText = "Add"
    getConsts();
    getAppls();
  }

  let getConsts = function() {
    $http({
       method: 'GET',
       url: baseUrl + 'const'
    }).then(function(reply){
       $scope.consts = reply.data;
       $scope.constNameList = [];
       for (let constInd = 0; constInd < reply.data.length; constInd ++)
         $scope.constNameList.push(reply.data[constInd].name)
    })
  }

  $scope.cancelAppl = function() {
    $scope.userObjectA = {};
  }

  $scope.removeAppl = function(applId) {
    $http({
      method: 'DELETE',
      url: baseUrl + 'applicability/' + applId
    }).then(function(reply) {
      getAppls();
    })
  }

  $scope.addAppl = function() {
    $http({
      method: 'POST',
      url: baseUrl + 'applicability',
      data: $scope.userObjectA
    }).then(function(reply) {
      getAppls();
      $scope.cancelAppl();
    })
  }

  let getAppls = function() {
    $http({
       method: 'GET',
       url: baseUrl + 'applicability'
    }).then(function(reply){
       $scope.appls = reply.data;
    })
  }

  $scope.prepareEditConst = function(constObject) {
    $scope.userObjectC = Object.assign({}, constObject);
    $scope.editConstText = 'Upd';
  }

  $scope.cancelConst = function() {
    $scope.userObjectC = {};
    $scope.selectedFreq = "";
    $scope.editConstText = 'Add';
  }

  $scope.removeConst = function(constName) {
    $http({
      method: 'DELETE',
      url: baseUrl + 'const/' + constName
    }).then(function(reply){
      getConsts();
      getAppls();
    })
  }

  let postConst = function() {
    $http({
      method: 'POST',
      url: baseUrl + 'const',
      data: $scope.userObjectC
    }).then(function(reply){
      getConsts();
      $scope.cancelConst();
    })
  }

  let updConst = function() {
    $http({
      method: 'PATCH',
      url: baseUrl + 'const/' + $scope.userObjectC.name,
      data: $scope.userObjectC
    }).then(function(reply){
      getConsts();
      $scope.cancelConst();
    })
  }

  $scope.addConst = function() {
    if ($scope.editConstText === 'Add')
      postConst();
    else
      updConst();
  }

  $scope.closeNav = function() {
    document.getElementById("sideNav").style.width = "0";
    $scope.getMonthData();
    $scope.getDayData();
  }

  $scope.init = function() {
    let curDate = new Date;
    $scope.editTextInc = "Add";
    $scope.editTextInc = "Add";
    $scope.monthsText = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
    $scope.selectedYear = $scope.curYear = curDate.getYear() + 1900;
    $scope.curMonth = curDate.getMonth();
    $scope.selectedDay = $scope.curDay = curDate.getDate();
    $scope.selectedMonth = $scope.monthsText[$scope.curMonth];
    $http({
      method: 'GET',
      url: baseUrl + 'dates?values=years'
    }).then(function(reply) {
        res = [];
        for (let yIndex = 0; yIndex < reply.data.length; yIndex++)
            res.push(reply.data[yIndex].year);
        $scope.years = res;
        $scope.yearSelection = $scope.years[$scope.years.indexOf($scope.curYear)];
        $scope.getMonthData();
        $scope.getDayData();
    });
  }

  function daysInMonth (month, year) {
    return 32 - new Date(year, month, 32).getDate();
  }

  $scope.goToToday = function() {
    $scope.selectedMonth = $scope.selectedMonth = $scope.monthsText[$scope.curMonth];
    $scope.selectedYear = $scope.yearSelection = $scope.years[$scope.years.indexOf($scope.curYear)];
    $scope.selectedDay = $scope.curDay;
    $scope.getMonthData();
  }

  $scope.getMonthData = function() {
    $http({
      method: 'GET',
      url: baseUrl + 'month_values?year=' + $scope.selectedYear + '&month=' + ($scope.monthsText.indexOf($scope.selectedMonth) + 1)
    }).then(function(reply){
      $scope.month_data = reply.data;
      for (let dayInd = 1; dayInd <= Object.keys($scope.month_data).length; dayInd++) {
          let _style;
          if ($scope.selectedYear < $scope.curYear) {
              _style = 'color: blue';
          } else if ($scope.selectedYear > $scope.curYear) {
              _style = 'color: black';
          } else if ($scope.monthsText.indexOf($scope.selectedMonth) < $scope.curMonth) {
              _style = 'color: blue';
          } else if ($scope.monthsText.indexOf($scope.selectedMonth) > $scope.curMonth) {
              _style = 'color: black';
          } else if (dayInd < $scope.curDay) {
              _style = 'color: blue';
          } else if (dayInd > $scope.curDay) {
              _style = 'color: black';
          } else {
              _style = 'color: green; font-weight: bold';
          }
          if ($scope.selectedDay === dayInd)
              _style += '; background-color: #FAFAFA';
          $scope.month_data[dayInd]['style'] = _style;
      }
    })
  }

  $scope.switchDate = function(action, inst) {
    //  good example for do recursion
     let isFirstYear = $scope.years.indexOf($scope.selectedYear) === 0;
     let isLastYear = $scope.years.indexOf($scope.selectedYear) === $scope.years.length - 1;
     let isFirstMonth = $scope.selectedMonth === $scope.monthsText[0];
     let isLastMonth = $scope.selectedMonth === $scope.monthsText[11];
     let isFirstDay = $scope.selectedDay === 1;
     let isLastDay = Object.keys($scope.month_data).length === $scope.selectedDay;

     if (inst === 'year')
        if (action === 'dec')
           $scope.selectedYear = !isFirstYear ? $scope.selectedYear - 1: $scope.selectedYear;
        else if (action === 'inc')
           $scope.selectedYear = !isLastYear ? $scope.selectedYear + 1: $scope.selectedYear;
     if (inst === 'month')
        if (action === 'dec'){
          if (!isFirstMonth)
            $scope.selectedMonth = $scope.monthsText[$scope.monthsText.indexOf($scope.selectedMonth) - 1];
          else if (!isFirstYear) {
            $scope.selectedMonth = $scope.monthsText[11];
            $scope.selectedYear = $scope.selectedYear - 1;
          }
        } else if (action === 'inc') {
          if (!isLastMonth)
            $scope.selectedMonth = $scope.monthsText[$scope.monthsText.indexOf($scope.selectedMonth) + 1];
          else {
            if (!isLastYear) {
              $scope.selectedYear = $scope.selectedYear + 1;
              $scope.selectedMonth = $scope.monthsText[0];
            }
          }
        }
     if (inst === 'day')
        if (action === 'dec') {
            if (!isFirstDay)
                $scope.selectedDay = $scope.selectedDay - 1;
            else if (!isFirstMonth) {
                $scope.selectedMonth = $scope.monthsText[$scope.monthsText.indexOf($scope.selectedMonth) - 1];
                $scope.selectedDay = daysInMonth($scope.monthsText.indexOf($scope.selectedMonth), $scope.selectedYear);
            } else if (!isFirstYear) {
                $scope.selectedYear = $scope.selectedYear - 1;
                $scope.selectedMonth = $scope.monthsText[11];
                $scope.selectedDay = daysInMonth($scope.monthsText.indexOf($scope.selectedMonth), $scope.selectedYear);
            }
        }
        else if (action === 'inc') {
            if (!isLastDay)
                $scope.selectedDay = $scope.selectedDay + 1;
            else if (!isLastMonth) {
                $scope.selectedMonth = $scope.monthsText[$scope.monthsText.indexOf($scope.selectedMonth) + 1];
                $scope.selectedDay = 1;
            } else if (!isLastYear) {
                $scope.selectedYear = $scope.selectedYear + 1;
                $scope.selectedMonth = $scope.monthsText[0];
                $scope.selectedDay = 1;
            }
        }
     $scope.getMonthData();
  }

  $scope.getDayData = function() {
    $http({
        method: 'GET',
        url: baseUrl + 'record?day=true&apply_date_gte=' + getCurSelectedDate()
    }).then(function(reply){
        $scope.day_diffs = reply.data;
        $scope.editTextInc = "Add";
        $scope.editTextDec = 'Add';
        $scope.userObjectE = {};
        $scope.userObjectI = {};
    })
  }

  $scope.onSelectDay = function(selectedDayIndex) {
    $scope.selectedDay = selectedDayIndex;
    $scope.getMonthData();
    $scope.getDayData();
  }

  $scope.cancelInc = function() {
    $scope.userObjectI = {'Value': "", "Desc": ""};
    $scope.getDayData();
  }

  $scope.cancelDec = function() {
    $scope.userObjectE = {'Value': "", "Desc": ""};
    $scope.getDayData();
  }

  let updateDB = function() {
    $http({
        method: 'POST',
        url: baseUrl + 'month'
    }).then(function(reply){
        $scope.getMonthData();
    })
  }

  let getCurSelectedDate = function() {
    return String($scope.selectedYear) + '-' + ('00' + ($scope.monthsText.indexOf($scope.selectedMonth) + 1)).slice(-2)
    + '-' + ('00' + $scope.selectedDay).slice(-2);
  }

  let addRecord = function(recordObject) {
    $http({
        method: 'POST',
        url: baseUrl + 'record',
        data: {
            'value': recordObject.Value,
            'comment': recordObject.Desc !== undefined ? recordObject.Desc : "",
            'date': getCurSelectedDate()
        },
        headers: {'Content-Type': 'application/json'}
    }).then(function(reply){
        updateDB();
        $scope.getDayData();
    })
  }

  let updRecord = function(recordObject) {
    $http({
        method: 'PATCH',
        url: baseUrl + 'record/' + recordObject.Id,
        data: {
            'value': recordObject.Value,
            'comment': recordObject.Desc !== undefined ? recordObject.Desc : "",
            'date': getCurSelectedDate()
        },
        headers: {'Content-Type': 'application/json'}
    }).then(function(reply){
        updateDB();
        $scope.getDayData();
    })
  }

  $scope.addInc = function() {
    if ($scope.userObjectI.Id !== undefined)
        updRecord($scope.userObjectI);
    else
        addRecord($scope.userObjectI);
    $scope.cancelInc();
  }

  $scope.addDec = function() {
    $scope.userObjectE.Value = -$scope.userObjectE.Value;
    if ($scope.userObjectE.Id !== undefined)
        updRecord($scope.userObjectE)
    else
        addRecord($scope.userObjectE);
    $scope.cancelDec();
  }

  $scope.removeRec = function(recordId) {
    $http({
        method: 'DELETE',
        url: baseUrl + 'record/' + recordId
    }).then(function(reply){
        updateDB();
        $scope.getDayData();
    })
  }

  $scope.prepareEditInc = function(recordObject) {
    $scope.editTextInc = 'Upd';
    $scope.userObjectI.Value = recordObject.value;
    $scope.userObjectI.Desc = recordObject.comment;
    $scope.userObjectI.Id = recordObject.id;
  }

  $scope.prepareEditDec = function(recordObject) {
    $scope.editTextDec = 'Upd';
    $scope.userObjectE.Value = - recordObject.value;
    $scope.userObjectE.Desc = recordObject.comment;
    $scope.userObjectE.Id = recordObject.id;
  }

//  $scope.init();
//  $scope.cur_year = 2019;
//  $scope.loadingFromFileComplete = false;
//
//  let userName = '';
//  let months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
//
//  $scope.months_selections = [];
//  for (let monthInd = 0; monthInd < months.length; monthInd++) {
//    $scope.months_selections[monthInd] = {"name": months[monthInd]};
//  }
//
//  var preInitMonthsSelections = function() {
//    for (let selectedMonth of $scope.months_selections) {
//      selectedMonth['days'] = [];
//      let _daysInMonth = daysInMonth($scope.months_selections.indexOf(selectedMonth), $scope.cur_year);
//      for (let dayInd = 0; dayInd < _daysInMonth; dayInd++) {
//        selectedMonth['days'][dayInd] = {'day_start': 0.0, 'day_end': 0.0};
//      }
//    }
//  }
//
//  var curDate = new Date;
//  let daysInCurSelectedMonth;
//
//  $scope.curSelectedMonthInd = $scope.curMonthInd = curDate.getMonth();
//  $scope.curSelectedMonth = $scope.curMonth = $scope.months_selections[$scope.curMonthInd].name;
//
//  $scope.diffs = {'2019': {}};
//  for (let monthInd = 0; monthInd < months.length; monthInd++) {
//    $scope.diffs['2019'][months[monthInd]] = {};
//    for (let dayInd = 0; dayInd < daysInMonth(monthInd, $scope.cur_year); dayInd++){
//      $scope.diffs['2019'][months[monthInd]][dayInd] = [];
//    }
//  }
//  $scope.curSelectedDayInd = $scope.curDayInd = curDate.getDate() - 1;
//  preInitMonthsSelections();
//  init();
//
//  function init() {
//    $scope.AddTextI = $scope.AddTextE = $scope.addText;
//    $scope.extendedMode = navigator.language !== "en-US" ? false : true;
//    $scope.extendedVisibility = $scope.extendedMode ? "visibility:visible" : "visibility:collapse; width:0px";
//    upd_days_data();
//  }
//
//  $scope.gotoPrevDay = function() {
//    if ($scope.curSelectedDayInd-1 > 0)
//      $scope.curSelectedDayInd--;
//    else if ($scope.curSelectedMonthInd > 0){
//      $scope.curSelectedMonthInd--;
//      $scope.curSelectedDayInd = daysInMonth($scope.curSelectedMonthInd, $scope.cur_year) - 1;
//      $scope.curSelectedMonth = months[$scope.curSelectedMonthInd];
//    }
//    $scope.upd_day_diffs();
//    upd_days_data();
//    document.getElementById("_month_selection").value = $scope.monthsText[$scope.curSelectedMonthInd];
//  }
//
//  $scope.gotoNextDay = function() {
//    if ($scope.curSelectedDayInd+1 < daysInMonth($scope.curSelectedMonthInd, $scope.cur_year))
//      $scope.curSelectedDayInd++;
//    else {
//      $scope.curSelectedMonthInd++;
//      $scope.curSelectedDayInd = 0;
//      $scope.curSelectedMonth = months[$scope.curSelectedMonthInd];
//    }
//    $scope.upd_day_diffs();
//    upd_days_data();
//    document.getElementById("_month_selection").value = $scope.monthsText[$scope.curSelectedMonthInd];
//  }
//
//  $scope.switchExtended = function() {
//    $scope.extendedMode = !$scope.extendedMode;
//    $scope.extendedText = $scope.extendedMode ? $scope.extendedTextExt : $scope.extendedTextUsual;
//    $scope.extendedVisibility = $scope.extendedMode ? "visibility:visible" : "visibility:collapse; width:0px";
//    upd_days_data();
//  }
//
//  function upd_days_data() {
//    for (let mainMonthInd = $scope.curSelectedMonthInd; mainMonthInd < 12; mainMonthInd++)
//    {
//      let month_data = $scope.months_selections[mainMonthInd]['days'];
//      for (let dayInd = 0; dayInd < month_data.length; dayInd++) {
//        let isFirstMonth = $scope.curSelectedMonthInd === 0;
//        let isFirstDay = dayInd === 0;
//
//        if (dayInd < daysInMonth($scope.curSelectedMonthInd, $scope.cur_year)) {
//          if (isFirstMonth && isFirstDay) month_data[dayInd]["day_start"] = 0.0;
//          else if (isFirstDay) {
//            try {
//              let prevMonth = $scope.months_selections[$scope.curSelectedMonthInd - 1]['days'];
//              month_data[dayInd]["day_start"] = prevMonth[prevMonth.length - 1]['day_end'];
//            } catch {
//              month_data[dayInd]["day_start"] = "error";
//            }
//          } else {
//            try {
//              let prevDay = month_data[dayInd - 1];
//              month_data[dayInd]["day_start"] = prevDay['day_end'];
//            } catch {
//              month_data[dayInd]["day_start"] = "error";
//            }
//          }
//
//          month_data[dayInd]["day_end"] = month_data[dayInd]['day_start'];
//          let curDiffs = $scope.diffs[$scope.cur_year][$scope.curSelectedMonth];
//          let curIncomes = 0;
//          let curExpenses = 0;
//          if (curDiffs !== undefined){
//            curDiffs = curDiffs[dayInd];
//            if (curDiffs !== undefined){
//                let curDiffsSum = 0.0;
//                for (let curDiff of curDiffs) {
//                  curDiffsSum += curDiff.value;
//                  if (curDiff.value > 0) curIncomes += curDiff.value; else curExpenses -= curDiff.value;
//                }
//                month_data[dayInd]["day_end"] += curDiffsSum;
//                month_data[dayInd]["incomes"] = curIncomes;
//                month_data[dayInd]["expenses"] = curExpenses;
//              }
//          }
//        }
//      }
//      if (mainMonthInd == $scope.curSelectedMonthInd){
//        $scope.month_data = month_data;
//        for (let dayInd = 0; dayInd < month_data.length; dayInd++) {
//          if ($scope.curSelectedMonthInd < $scope.curMonthInd) {
//            _style = "color: blue";
//          } else if ($scope.curSelectedMonthInd == $scope.curMonthInd)
//          {
//            if (dayInd < $scope.curDayInd)
//              _style = "color: blue";
//            else if (dayInd == $scope.curDayInd)
//              _style = "color: green; font-weight: bold";
//            else
//              _style = "color: black";
//          } else {
//            _style = "color: black";
//          }
//          if (dayInd == $scope.curSelectedDayInd)
//            _style += '; background-color: #FAFAFA';
//          month_data[dayInd]['style'] = _style;
//        }
//      }
//    }
//  }
//
//  $scope.upd_day_diffs = function() {
//    $scope.day_diffs = [];
//    let curDiffs = $scope.diffs[$scope.cur_year][$scope.curSelectedMonth];
//    if (curDiffs !== undefined){
//      curDiffs = curDiffs[$scope.curSelectedDayInd];
//      if (curDiffs !== undefined){
//          for (let curDiff of curDiffs) {
//            $scope.day_diffs[curDiffs.indexOf(curDiff)] = {"value": curDiff.value, "desc": curDiff.desc}
//          }
//        }
//    }
//    upd_days_data();
//  }
//
//  $scope.onSelectAnotherMonth = function() {
//    if ($scope.curSelectedMonthInd !== $scope.monthsText.indexOf($scope.month_selection)) {
//      $scope.curSelectedMonthInd = $scope.monthsText.indexOf($scope.month_selection);
//      $scope.curSelectedMonth = months[$scope.curSelectedMonthInd];
//      $scope.curSelectedDayInd = 0;
//      upd_days_data();
//      $scope.upd_day_diffs();
//    }
//  }
//
//  $scope.exportToFile = function(){
//    var dataToSave = {"2019": {
//      "months": $scope.months_selections,
//      "diffs": $scope.diffs
//    }};
//    $http({
//      method: 'POST',
//      data: dataToSave,
//      url: 'http://192.168.1.229:5000/stat_file?name=' + userName
//    }).then(function successCallback(response) {
//        alert('success');
//    });
//  }
//
//  $scope.onSelectDay = function(selectedDayInd) {
//    $scope.curSelectedDayInd = selectedDayInd;
//    $scope.upd_day_diffs();
//  }
//
//  $scope.removeDiff = function(diffIndToRemove) {
//    $scope.diffs[$scope.cur_year][$scope.curSelectedMonth][$scope.curSelectedDayInd].splice(diffIndToRemove, 1);
//    $scope.upd_day_diffs();
//    upd_days_data();
//  }
//
//  $scope.removeDiffIncome = $scope.removeDiffExpense = $scope.removeDiff;
//
//  var replaceIndex = undefined;
//
//  $scope.prepareReplaceDiffIncome = function(diffIndex) {
//    prepareReplaceDiff(diffIndex, 'new_value_i', 'new_desc_i', $scope.AddTextI);
//  }
//
//  $scope.prepareReplaceDiffExpense = function(diffIndex) {
//    prepareReplaceDiff(diffIndex, 'new_value_e', 'new_desc_e', $scope.AddTextE, true);
//  }
//
//  prepareReplaceDiff = function(diffIndex, val_id, desc_id, addTextVar, expenseFlag) {
//    daysDiffs = $scope.diffs[$scope.cur_year][$scope.curSelectedMonth][$scope.curSelectedDayInd];
//    replaceIndex = diffIndex;
//    if (expenseFlag !== undefined && expenseFlag === true)
//      document.getElementById(val_id).value = -daysDiffs[diffIndex].value;
//    else
//      document.getElementById(val_id).value = daysDiffs[diffIndex].value;
//    document.getElementById(desc_id).value = daysDiffs[diffIndex].desc;
//    if (expenseFlag !== undefined && expenseFlag === true)
//      $scope.AddTextE = $scope.saveText;
//    else
//      $scope.AddTextI = $scope.saveText;
//  }
//
//  $scope.replaceDiffIncome = function() {
//    replaceDiff("new_value_i", "new_desc_i", $scope.AddTextI);
//  }
//
//  $scope.replaceDiffExpense = function() {
//    replaceDiff("new_value_e", "new_desc_e", $scope.AddTextE, true);
//  }
//
//  $scope.enterUserName = function() {
//    userName = document.getElementById('user_name').value;
//    $http({
//      method: 'GET',
//      url: 'http://192.168.1.229:5000/stat_file?name=' + userName
//    }).then(function successCallback(response) {
//          $scope.months_selections = response.data[$scope.cur_year]['months'];
//          $scope.diffs = response.data[$scope.cur_year]['diffs'];
//          upd_days_data();
//          $scope.upd_day_diffs();
//          $scope.loadingFromFileComplete = true;
//      });
//  }
//
//  replaceDiff = function(val_id, desc_id, addTextVar, expenseFlag) {
//    strValue = document.getElementById(val_id).value.replace(',', '.');
//    newValue = parseFloat(strValue);
//    if (!newValue) alert($scope.badValueText); else {
//      if (expenseFlag !== undefined && expenseFlag === true) newValue = -newValue;
//      newDesc = document.getElementById(desc_id).value;
//      if (replaceIndex === undefined) {
//          daysDiffs = $scope.diffs[$scope.cur_year][$scope.curSelectedMonth][$scope.curSelectedDayInd];
//          daysDiffs[daysDiffs.length] = {"value": newValue, "desc": newDesc};
//        } else {
//          daysDiffs = $scope.diffs[$scope.cur_year][$scope.curSelectedMonth][$scope.curSelectedDayInd];
//          daysDiffs[replaceIndex] = {"value": newValue, "desc": newDesc};
//        }
//        $scope.upd_day_diffs();
//        upd_days_data();
//        document.getElementById(val_id).value = "";
//        document.getElementById(desc_id).value = "";
//        replaceIndex = undefined;
//      if (expenseFlag !== undefined && expenseFlag === true)
//        $scope.AddTextE = $scope.addText;
//      else
//        $scope.AddTextI = $scope.addText;
//    }
//  }
//
//  $scope.cancelReplaceDiffIncome = function() {
//    cancelReplaceDiff("new_value_i", "new_desc_i", $scope.AddTextI);
//  }
//
//  $scope.cancelReplaceDiffExpense = function() {
//    cancelReplaceDiff("new_value_e", "new_desc_e", $scope.AddText, true);
//  }
//
//  cancelReplaceDiff = function(val_id, desc_id, addTextVar, expenseFlag) {
//    document.getElementById(val_id).value = "";
//    document.getElementById(desc_id).value = "";
//    replaceIndex = undefined;
//    if (expenseFlag !== undefined && expenseFlag === true)
//      $scope.AddTextE = $scope.addText;
//    else
//      $scope.AddTextI = $scope.addText;
//  }


});