from functools import wraps
from logging import Logger

logger = Logger('errors')


def excWrap(wrapFunc):

    @wraps(wrapFunc)
    def _wrap(*arg, **argv):
        try:
            return wrapFunc(*arg, **argv)
        except Exception as e:
            logger.debug(str(e))

    return _wrap()

